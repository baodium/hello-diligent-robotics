# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.


## Deployed application on production environment
http://wale--12192022.s3-website-us-east-1.amazonaws.com/


## Deployed application in staging environment
http://wale--12192022-staging.s3-website-us-east-1.amazonaws.com/