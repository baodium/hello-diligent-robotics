import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src="https://assets.rbl.ms/25591258/origin.jpg" style={{ width: "20%", height: "25%"}} className="App-logo" alt="logo" />
        <br /><br /><br />
        <p>
          Thanks for giving me the opportunity to inteview with you! :) 
          This is exciting!
        </p>
        <p>
          <b>Company:</b> <i>Diligent Robotic </i><br/>
          <b>Candidate:</b> <i>Adewale Obadimu </i>
        </p>
        
      </header>
    </div>
  );
}

export default App;
